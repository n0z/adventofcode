with open("input","r") as f:
    spreadsheet=f.read().split("\n")
spreadsheet=[list(map(int,line.split())) for line in spreadsheet]

checksum_p1=0; checksum_p2=0
for row in spreadsheet:
    min_value=min(row); max_value=max(row)
    checksum_p1+=max_value-min_value

    i=0; found=False
    while i<len(row) and not found: 
        for x in range(i+1,len(row)):
            nums=[]; nums.append(row[i]);nums.append(row[x])
            nmax=max(nums); nmin=min(nums)
            if nmax%nmin==0: checksum_p2+=nmax//nmin; found=True; break  
        i+=1    
                            
print("Part1 -- What is the checksum for the spreadsheet in your puzzle input? "
      +str(checksum_p1))
print("Part2 -- What is the sum of each row's result in your puzzle input? "
      +str(checksum_p2))
