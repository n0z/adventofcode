with open("input","r") as f:
    banks=f.read().split()
banks=[int(v) for v in banks]

configurations=[]
cycles=0
configurations.append(banks.copy())
while True:
    index=banks.index(max(banks))
    value=banks[index]
    banks[index]=0
    while value>0:
        index=(index+1)%len(banks)
        banks[index]=banks[index]+1
        value-=1
    cycles+=1
    if banks in configurations: break
    configurations.append(banks.copy())

print("Part1 --  How many redistribution cycles must be completed before\
a configuration is produced that has been seen before? "+str(cycles))
print("Part2 -- How many cycles are in the infinite loop that arises from the\
configuration in your puzzle input? "+str(cycles-configurations.index(banks)))
