with open("input","r") as f:
   captcha=f.read()

i=0; size=len(captcha); forward=size//2
solution_p1=0; solution_p2=0
while i<size:
    if captcha[i]==captcha[(i+1)%size]: solution_p1+=int(captcha[i])
    if captcha[i]==captcha[(forward+i)%size]: solution_p2+=int(captcha[i])
    i+=1

print("Part1 -- What is the solution to your captcha? "
      +str(solution_p1))
print("Part2 -- What is the solution to your new captcha? "
      +str(solution_p2))

