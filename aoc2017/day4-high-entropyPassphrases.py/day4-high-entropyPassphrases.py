with open("input","r") as f:
    passphrases=f.read().split("\n")

passphrases_valids=0
passphrases_valids_p2=0
for passphrase in passphrases:
    words=passphrase.split()
    valid_p1=True
    for word in words:
        if passphrase.count(word)>1: valid_p1=False; break
    if valid_p1: passphrases_valids+=1

    valid_p2=True
    sortlist=[''.join(sorted(word)) for word in words]
    for word in sortlist:
        if sortlist.count(word)>1: valid_p2=False;break
    if valid_p2: passphrases_valids_p2+=1

print("Part1 -- How many passphrases are valid? "+str(passphrases_valids))
print("Part2 -- Under this new system policy, how many passphrases are valid? "
      +str(passphrases_valids_p2))
        
