
def compute(instructions: list, mode:str) -> int:
    i=0
    steps=0
    while i<len(instructions):
        new_i=instructions[i]
        if mode=="part2":
            if instructions[i]>=3: instructions[i]-=1
            else: instructions[i]+=1
        else: instructions[i]+=1
        i+=new_i
        steps+=1
    return steps 

with open("input","r") as f:
    instructions=f.read().split("\n")
instructions=[int(instruction) for instruction in instructions]
instructions_p2=instructions.copy()

print("Part1 -- How many steps does it take to reach the exit? "
      +str(compute(instructions,"part1")))
print("Part2 -- How many steps does it now take to reach the exit? "
      +str(compute(instructions_p2,"part2")))
