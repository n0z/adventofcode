input = open("input","r")
assignments = input.readlines()
input.close()

overlapsPart1 = 0
overlapsPart2 = 0
for assignment in assignments:
    s=assignment.strip().split(',')
    elf1=s[0].split('-')
    elf2=s[1].split('-')
    #PART 1 -- How many assignment pairs does one range fully contain the other?
    if ((int(elf1[0])<=int(elf2[0]) and int(elf1[1])>=int(elf2[1])) or
        (int(elf2[0])<=int(elf1[0]) and int(elf2[1])>=int(elf1[1]))):
        overlapsPart1+=1
    #PART 2 - How many assignment pairs do the ranges overlap?
    if ((int(elf1[0])<=int(elf2[0]) and int(elf1[1])>=int(elf2[1])) or
        (int(elf2[0])<=int(elf1[0]) and int(elf2[1])>=int(elf1[1])) or
        (int(elf1[1])>=int(elf2[0]) and int(elf1[0])<=int(elf2[1]))):
        overlapsPart2+=1
        
print("Part1 -- overlaps: " + str(overlapsPart1))
print("Part2 -- overlaps: " + str(overlapsPart2))
