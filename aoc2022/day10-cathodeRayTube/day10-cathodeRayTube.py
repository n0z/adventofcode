
with open("input","r") as f:
    instructions =f.read().split("\n")

X=1
cycle=0
cv=[] #Cycle and X Value array
CRT=[]
for i in range(0,6):
    c=[]
    for i in range(0,40):
        c.append("")
    CRT.append(c)


for inst in instructions:
    if inst.split()[0] == "noop":
        cycle+=1
        cv.append([cycle,X])
    else:
        for i in range(0,2):
            cycle+=1
            cv.append([cycle,X])
        X+=int(inst.split()[1])


sum=0
for i in range(19,220,40):
   sum+=cv[i][0]*cv[i][1] 

print("PART 1 -- The sum of these six signal strengths is "+str(sum))

cont=0
r=0
c=0
for i in range(0,len(cv)):
    cont+=1
    if c==(cv[i][1])-1 or c==(cv[i][1])+1 or c== cv[i][1]:
        CRT[r][c]="#"
    else:
        CRT[r][c]="."
    c+=1
    if(cont==40): cont=0; r+=1; c=0


print("Part2 -- What eight capital letters appear on your CRT?")
for line in CRT:
    for c in line:
        print(c,end="  ")
    print("\n")
