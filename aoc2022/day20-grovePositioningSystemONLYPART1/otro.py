buf = [(pos, int(line)) for pos, line in enumerate(open('input').readlines())]
print("buf size ori: "+str(len(buf)))
cont=0
for orig_pos, number in buf.copy(): # traverse in input order
    cont+=1
    #print(buf)
    #print("orig_pos: "+str(orig_pos)+" ,number->"+str(number))
    from_idx = buf.index((orig_pos, number))
    #print("from_idx: "+str(from_idx))
    to_idx = (from_idx + number) % (len(buf)-1)
    #print("to_idx: "+str(to_idx))
    #print("to_idx:"+str(to_idx))
    buf.insert(to_idx, buf.pop(from_idx))
print(cont)
zero_idx = [x[1] for x in buf].index(0)
print("zero_idx: "+str(zero_idx))
print(sum(buf[(zero_idx+offset)%len(buf)][1] for offset in [1000,2000,3000]))
