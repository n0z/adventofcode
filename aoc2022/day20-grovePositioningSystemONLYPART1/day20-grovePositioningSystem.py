
def mix(numbersOri):
    numbers= numbersOri.copy()
    size=len(numbers)
    
    for number in numbersOri:
        i= numbers.index(number)
        numbers.pop(i)
        x = (i+number[1])%(size-1)
        numbers.insert(x,number)

    return numbers


if __name__ == "__main__":

    with open("input2","r") as f:
        lines = f.read().splitlines()
    
    numbersOri =[]
    for i, number in enumerate(lines):
        numbersOri.append((i,int(number)))

    #Part1
    numbers=mix(numbersOri)
    zero= [n[1] for n in numbers].index(0)
    a,b,c=[numbers[(zero+offset)%len(numbers)][1] for offset in [1000,2000,3000]]
    print("Part1 -- What is the sum of the three numbers that form the grove coordinates? "+str(a+b+c))

    #Part2
    key=811589153
    numbersMOD = []
    for number in numbersOri:
        a=list(number)
        a[1]*=key
        number=tuple(a)
        numbersMOD.append(number)
    print("initial state")
    print(numbersMOD)
    print("\n")
    
    for i in range(0,10):
        #numbers= numbersMOD.copy()
        size=len(numbersMOD)
        
        for i,number in numbersMOD:
            #i= numbers.index(number)
            old_x=numbersMOD.index((i,number))
            numbers.pop(old_x)
            x = (old_x+number)%(size-1)
            numbers.insert(x,number)

        
        print("After "+str(i+1)+" rounds of mixing:")
        numbersMOD = mix(numbersMOD)
        print(numbersMOD)
        print("\n")
    print("##############")
    print(numbersMOD)
    zero= [n[1] for n in numbersMOD].index(0)
    a,b,c=[numbersMOD[(zero+offset)%len(numbersMOD)][1] for offset in [1000,2000,3000]]
    print(a)
    print(b)
    print(c)
    print("Part2 -- What is the sum of the three numbers that form the grove coordinates? "+str(a+b+c))
