import string

input = open("input","r")
rucksacks = input.readlines()
input.close()

#Create dictionary with priorities values for each letter
priority = {}
n = 1
for letter in string.ascii_lowercase:
    priority[letter] = n
    n+=1
for letter in string.ascii_uppercase:
    priority[letter] = n
    n+=1


#PART 1 -- Sum of the priorities of the item that appear in both
#comparment of each rucksuck.
sumPriorities = 0
for rs in rucksacks:
    objects = rs.strip()
    n = len(objects)
    compartment1=objects[0:n//2]
    compartment2=objects[n//2:]
    for object in compartment1:
        if object in compartment2:
           sumPriorities+=priority[object]
           break
print("PART1 Sum of priorities of the item appear in both compartment of each rucksuck: " + str(sumPriorities))

#PART2 -- Badge (item) that appear in the 3 rucksucks of a group of 3 elves.
sumPrioPart2 = 0
n = len(rucksacks)    
for group in range(0,n,3):
    for object in rucksacks[group]:
        if object in rucksacks[group+1] and object in rucksacks[group+2]:
           sumPrioPart2+=priority[object]
           break
print("PART2 - Sum of priorities of badges: " + str(sumPrioPart2))
    
