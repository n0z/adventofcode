import copy

def operate(a,b,op):
    if op=="+": return a+b
    elif op=="-": return a-b
    elif op=="*": return a*b
    elif op=="/": return a//b

def part1(monkeys):
    while monkeys.get("root")[1] != True:
        for monkey,values in monkeys.items():
            if len(values[0]) == 1 and values[1]==False:
                values[1] = True
            elif len(values[0]) == 3:
                if monkeys.get(values[0][0])[1] and monkeys.get(values[0][2])[1]:
                    a= monkeys.get(values[0][0])[0][0]
                    b= monkeys.get(values[0][2])[0][0]
                    op= values[0][1]
                    monkeys[monkey]=[[operate(int(a),int(b),op)],True]
    print(monkeys.get("root")[0][0])


def part2(monkeys):
    monkeys.get("root")[0][1]="="
    print("ROOT ORIGINAL-> "+str(monkeys.get("root")))
    while monkeys.get("root")[1] != True:
        for monkey,values in monkeys.items():
            #print(monkey+": "+str(values))
            if len(values[0]) == 1 and values[1]==False:
                values[1] = True
            elif len(values[0]) == 3:
                #print(monkeys.get(values[0][0])[1])
                if monkeys.get(values[0][0])!= None and monkeys.get(values[0][2])!=None:
                    #print("entra a pesar de los none")
                    #print(" en el mono: "+monkey)
                    if monkeys.get(values[0][0])[1] and monkeys.get(values[0][2])[1]:
                        a= monkeys.get(values[0][0])[0][0]
                        b= monkeys.get(values[0][2])[0][0]
                        op= values[0][1]
                        monkeys[monkey]=[[operate(int(a),int(b),op)],True,[a,op,b],values[0]]
                        #monkeys[monkey]=[[a,op,b],True]
                    #monkeys[monkey]=[[operate(int(a),int(b),op)],True]
    for monkey,values in monkeys.items():
        print(monkey+": "+str(values))

    monkey1=monkeys.get("root")[3][0]
    monkey2=monkeys.get("root")[3][2]
    print("monkey1: "+monkey1)
    print("monkey2: "+monkey2)
    monkey=""
    l = ["humn"]
    while l[-1]!="root":
        for monkey, values in monkeys.items():
            #print(monkey, values)
            if len(values)>2:
                if values[3][0]==l[-1] or values[3][2]==l[-1]:
                    l.append(monkey)
                    #if l[-1]=="humn":
                    #    n.append(values[2][1]);n.append(values[2][2])
                #elif values[3][2] == l[-1]: l.append(monkey); n.append(values[2][1]);n.append(values[2][2])
                #print(l)

    print(l)
    l.pop(0)
    l.reverse()
    print("l->"+str(l))
    #print(n)
    math=[]
    n=[]
    for x in l:
        if monkeys.get(x)[3][0] == "humn": a="x"
        else:
            if len(monkeys.get(monkeys.get(x)[3][0]))>2:
                a=monkeys.get(x)[3][0]
            else:
                a=monkeys.get(monkeys.get(x)[3][0])[0][0] 
        op=monkeys.get(x)[3][1]
        if monkeys.get(x)[3][2] == "humn": b="x"
        else:
            if len(monkeys.get(monkeys.get(x)[3][2]))>2:
                b = monkeys.get(x)[3][2]
            else:
                b = monkeys.get(monkeys.get(x)[3][2])[0][0]
        n.append([a,op,b]) 
    print("n->"+str(n))


    for a in reversed(n):
        print("a->"+str(a))
        if not a[0].isdigit() and a[0]!="x":
            i = l.index(a[0])
            a[0]=n[i]
            del n[i]
            del l[i]
        elif not a[2].isdigit() and a[2]!="x":
            i=l.index(a[2])
            a[2]=n[i]
            #n.pop(a)
            del n[i]
            del l[i]
    print(n)
        
        
with open("input","r") as f:
    monkeysO =f.read().splitlines()

monkeys={}
for monkey in monkeysO:
    monkeys[monkey.split(":")[0]]=[monkey.split(":")[1].strip().split(),False]
monkeys2=copy.deepcopy(monkeys)
part1(monkeys)
part2(monkeys2)
