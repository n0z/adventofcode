def calculate(a,b,sign):
    if b=='old': b=a
    if sign=='+': return (int(a)+int(b))
    elif sign=='-': return (int(a)-int(b))
    elif sign=='*': return (int(a)*int(b))
    elif sign=='/': return (int(a)//int(b))
   

def read_monkeys_input(list):
    monkeys = []
    for monkey in list:
        mon=[]
        m = monkey.split("\n")
        stItems=m[1].strip().split(":")[1].strip().split(", ")
        mon.append(stItems)
        operation=m[2].strip().split()[-3:]
        mon.append(operation)
        test=[]
        test.append(m[3].strip().split()[-1])
        test.append(m[4].strip().split()[-1])
        test.append(m[5].strip().split()[-1])
        mon.append(test)
        mon.append(0)
        monkeys.append(mon)
    return monkeys

#causeWorry true if monkey inspect element causeWorry(part1)
#and false if not (part2)
def monkeys_inspect_items(rounds: int,monkeys: list,causeWorry: bool):
    for i in range(0,rounds):
        for i in range(0,len(monkeys)):
            while len(monkeys[i][0]) > 0:
                monkeys[i][3]+=1
                item = monkeys[i][0][0]
                wlevel = calculate(item,monkeys[i][1][2],monkeys[i][1][1])
                if causeWorry: wlevel = wlevel//3
                if wlevel%int(monkeys[i][2][0]) == 0:
                    monkeys[int(monkeys[i][2][1])][0].append(wlevel) 
                else:
                    monkeys[int(monkeys[i][2][2])][0].append(wlevel)
                monkeys[i][0].pop(0)

def calculate_level_monkey_business(monkeys):
    max1=0
    max2=0
    for i in range(0,len(monkeys)):
        if monkeys[i][3]>max1: max2=max1; max1=monkeys[i][3]
    return max1*max2


if __name__ == "__main__":
    
    with open("input2","r") as f:
        list=f.read().split("\n\n")
        
    monkeys = read_monkeys_input(list)
    monkeysP2 = read_monkeys_input(list)
    print(monkeys)
    print("\n")
    print(monkeysP2)
    monkeys_inspect_items(20,monkeys,True)
    #monkeys_inspect_items(20,monkeysP2,True)
    print("Part1 -- Level of monkey business after 20 rounds: "+str(calculate_level_monkey_business(monkeys)))
    print(monkeysP2)
    #monkeys_inspect_items(10000,monkeysP2,False)
    #print("Part2 -- Level of monkey business after 20 rounds: "+str(calculate_level_monkey_business(monkeysP2)))
    #print("Part2 -- Level of monkey business after 1000 rounds: "+str(calculate_level_monkey_business(monkeys)))
