
#Method for check variable lenmarker, in case of the aoc
#the part1 ask for a 4 character marker, but second part
#ask for a 14 character marker
def get_first_marker(stream,lenMarker):
    window = [None]*lenMarker
    
    #initial window setup
    for i in range(0,lenMarker):
        window[i]=stream[i]
        #initial window check, if all different then first marker
        #after 4 character
        #if len of window is the same as a set(all different characetrs)
        #of window
        
    if len(window) == len(set(window)):
        print(lenMarker)
    else:
        cont=lenMarker
        n = len(stream)

        for i in range(lenMarker,n):
            cont+=1
            for l in range(0,lenMarker):
                if l == lenMarker-1:
                    window[l] = stream[i]
                elif l+1<lenMarker:
                    window[l] = window[l+1]
                
            if len(window) == len(set(window)):
                print("First start-of-packet marker with "+str(lenMarker)+" characters after character -> "+ str(cont))
                break



if __name__ == "__main__":
    
    with open("input","r") as f:
        stream = f.read()

    #Part 1 - search start-of-packet marker with 4 different characters
    get_first_marker(stream,4)

    #Part 2 - search start-of-packet marker with 4 different characters
    get_first_marker(stream,14)
