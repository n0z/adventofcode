input = open("input", "r")
output = input.readlines()
input.close()


calories = 0
elfs = [] #list of calories of each elf(index)
for s in output:
    if s =="\n":
        elfs.append(calories)
        calories = 0
    else:
        calories += int(s)

#last elf endOffile
elfs.append(calories)

#Part 1 of day1
elfs.sort()
print("Total calories of the elf carrying the most calories: " + str(elfs[-1]))

#PART 2 of day1
#print(elfs)
print("Total calories of the three elfs carrying the most calories: " + str(elfs[-1] + elfs[-2] + elfs[-3]))

