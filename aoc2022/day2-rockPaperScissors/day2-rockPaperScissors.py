input = open("input", "r")
rounds = input.readlines()
input.close()

#PART 1 dictionaries
result = {"AX":3,"BY":3,"CZ":3,#draw situations
          "CX":6,"AY":6,"BZ":6,#wining situations
          "AZ":0,"BX":0,"CY":0}#lose situations

shapePoint = {"X":1,"Y":2,"Z":3}

#PART 2 dictionaries
shapePointPart2 = {"A":1,"B":2,"C":3} 
stratPoints = {"X":0,"Y":3,"Z":6}
#Wich shape we have to play
shapeByStrategie = {"AX":"C","BX":"A","CX":"B",
                    "AY":"A","BY":"B","CY":"C",
                    "AZ":"B","BZ":"C","CZ":"A"}

totalScorePart1 = 0
totalScorePart2 = 0
for round in rounds:
    #Part1
    play = round[0]+round[2]
    shape = round[2]
    totalScorePart1 += (result.get(play) + shapePoint.get(shape)) 

    #Part2 
    totalScorePart2 += (shapePointPart2.get(shapeByStrategie.get(play)) + stratPoints.get(shape))

print("Total score part1: " + str(totalScorePart1))
print("Total score part2: " + str(totalScorePart2))

#part 2


