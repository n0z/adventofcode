import copy

def get_max_X_and_Y(paths):
    X=0
    Y=0
    for path in paths:
        path=path.split("->")
        for p in path:
            x,y=p.split(",")
            if int(x)>X: X=int(x)
            if int(y)>Y: Y=int(y)

    return X,Y

def create_initial_grid(x,y):
    grid=[]
    for r in range(0,y+1):
        g=[]
        for c in range(0,x+1):
            g.append(".")
        grid.append(g)
    return grid

def parse_grid(grid,paths):
    for path in paths:
        path=path.split("->")
        for i in range(0,len(path)):
            if i+1<len(path):
                xIni,yIni=path[i].split(",")
                xFin,yFin=path[i+1].split(",")
                xIni=int(xIni); xFin=int(xFin); yIni=int(yIni); yFin=int(yFin)
                ini=-1
                fin=-1
                if xIni == xFin:
                    if yIni>yFin: ini=yFin; fin=yIni
                    else: ini=yIni; fin=yFin
                    for c in range(ini,fin+1):
                        grid[c][xIni]="#"
                elif yIni == yFin:
                    if xIni>xFin: ini=xFin; fin=xIni
                    else: ini=xIni; fin=xFin
                    for r in range(ini,fin+1):
                        grid[yIni][r]="#"
            
    return grid

def put_last_floor(grid,y):
    for i in range(0,len(grid[0])):
        grid[y+2][i]="#"

def count_units_sand(grid:list, part: str):
    posSand = []
    posSand.append(501)
    posSand.append(0)
    cont=0

    if part=="part1":
        while posSand[1]<len(grid)-1:
            posSand=drop_unit_sand(grid, posSand)
            if grid[posSand[1]][posSand[0]]=="o":cont+=1; posSand[0]=500; posSand[1]=0
    elif part=="part2":
        while grid[0][500]!="o":
            posSand=drop_unit_sand(grid, posSand)
            if grid[posSand[1]][posSand[0]]=="o":cont+=1; posSand[0]=500; posSand[1]=0

    return cont

def drop_unit_sand(grid: list, posSand: list):

    down=grid[posSand[1]+1][posSand[0]] 
    dleft=grid[posSand[1]+1][posSand[0]-1]
    dright=grid[posSand[1]+1][posSand[0]+1]
         
    if down != "." and dleft != "." and dright != ".":
        grid[posSand[1]][posSand[0]]="o"
    else:
        if down == ".":
            posSand[1]+=1
        elif dleft == ".":
            posSand[0]-=1
            posSand[1]+=1
        elif dright == ".":
            posSand[0]+=1
            posSand[1]+=1
    return posSand


if __name__ == "__main__":
    with open("input","r") as f:
        paths=f.read().split("\n")

    x,y=get_max_X_and_Y(paths)

    #Part1
    grid = create_initial_grid(x+50,y)
    grid = parse_grid(grid,paths)
    unitsOfSand = count_units_sand(grid, "part1") 
    print("Part1 -- How many units of sand come to rest before sand starts flowing into the abyss below? " + str(unitsOfSand))

    #for i in grid:
    #    print(i)
    #Part2
    grid = create_initial_grid(x+500,y+2)#We need two level extra to put a floor on the last one.
    grid = parse_grid(grid,paths)
    put_last_floor(grid,y)
    unitsOfSand = count_unit_sand_part2(grid,(500,0),y) 
    print("Part2 -- How many units of sand come to rest? "+str(unitsOfSand))
