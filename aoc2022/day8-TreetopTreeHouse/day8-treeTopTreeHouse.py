

#Check from the tree position(i,j) to the left of it
def isVisibleFromLeft(i,j,grid):
    treeH = grid[i][j] #Height of the tree
    n=j-1
    while n>=0 and treeH>grid[i][n]: n-=1
    if n<0: return True
    else: return False
    
#Check from the tree position(i,j) to the right of it
def isVisibleFromRight(i,j,grid):
    treeH = grid[i][j]
    numCols = len(grid[0])
    n=j+1
    while n<numCols and treeH>grid[i][n]: n+=1
    if n==numCols: return True
    else: return False

#Check from the tree position(i,j) to the up of it
def isVisibleFromUp(i,j,grid):
    treeH = grid[i][j]
    n=i-1
    while n>=0 and treeH>grid[n][j]: n-=1
    if n<0: return True
    else: return False

#Check from the tree position(i,j) to the down of it
def isVisibleFromDown(i,j,grid):
    treeH = grid[i][j]
    numRows = len(grid)
    n=i+1
    while n<numRows and treeH>grid[n][j]: n+=1
    if n==numRows: return True
    else: return False


def calculateScenicScore(i,j,grid):
    treeH = grid[i][j]
    numCols = len(grid[0])
    numRows = len(grid)
    scL = 0; scR = 0; scU = 0; scD = 0 

    n=j-1
    while n>=0:
        scL+=1
        if grid[i][n]>=treeH: break
        n-=1
        
    n=j+1
    while n<numCols:
        scR+=1
        if grid[i][n]>=treeH: break 
        n+=1
    
    n=i-1
    while n>=0:
        scU+=1
        if grid[n][j]>=treeH: break 
        n-=1

    n=i+1
    while n<numRows:
        scD+=1
        if grid[n][j]>=treeH: break 
        n+=1

    return (scL * scR * scU * scD)


if __name__ == "__main__":
    
    with open("input","r") as f:
        output=f.read().split("\n")
        
    grid=[]
    for line in output:
        c = []
        for t in line:
            c.append(t)
        grid.append(c)

    
    
    #All the edge trees all visible, so we start with that number of visible trees
    numRows=len(grid)
    numCols=len(grid[0])
    visibleTrees = (numCols*2+numRows*2)-4
    treesSelected = []
    for i in range(1,numRows-1):
        for j in range(1,numCols-1):
            if isVisibleFromLeft(i,j,grid):
                treesSelected.append([i,j])
                visibleTrees+=1
            elif isVisibleFromRight(i,j,grid):
                treesSelected.append([i,j])
                visibleTrees+=1
            elif isVisibleFromUp(i,j,grid):
                treesSelected.append([i,j])
                visibleTrees+=1
            elif isVisibleFromDown(i,j,grid):
                treesSelected.append([i,j])
                visibleTrees+=1

    print("Part1 -- How many trees are visible from outside the grid? " + str(visibleTrees))

    max = 0
    for tree in treesSelected:  
      sc = calculateScenicScore(tree[0],tree[1],grid)  
      if sc>max: max=sc

    print("Part2 -- What is the highest scenic score possible for any tree? "+str(max))


