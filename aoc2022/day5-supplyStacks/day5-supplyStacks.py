import re

def get_stacks(containers):
    revContainers = containers[::-1]
    lines = revContainers.splitlines()
    
    posInitial=int(lines[0].split()[0])
    stacks = [[] for _ in range(0,posInitial)]
    lines.pop(0)
    pos = posInitial
    for line in lines:
        i=0
        while i < len(line):
            if line[i] == ']':
                pos-=1
                stacks[pos].append(line[i+1]) 
                i+=4
            elif line[i] == ' ':
                pos-=1
                i+=4
        pos=posInitial
                
    return stacks

def move_containers(movements, stacks):

    ms = movements.split("\n")
    ms.pop()
    for m in ms:
        a=m.split(" ")
        if len(a) > 0:
            quantity=int(a[1])
            origin=int(a[3])-1
            dest=int(a[5])-1
            for i in range(0,quantity):
                container = stacks[origin].pop()
                stacks[dest].append(container)
        
    return stacks

def move_containers_part2(movements, stacks):
    
    ms = movements.split("\n")
    ms.pop()#Get rid off a empty string in the last line of movements of \n
    for m in ms:
        a=m.split(" ")
        if len(a) > 0:
            quantity=int(a[1])
            origin=int(a[3])-1
            dest=int(a[5])-1
            c = []
            for i in range(0,quantity):
                container = stacks[origin].pop()
                c.append(container)
            c.reverse()
            for x in c:
                stacks[dest].append(x)

    return stacks


if __name__ == "__main__":
    
    with open("input","r") as f:
        containers, movements = f.read().split("\n\n")

    stacks1 = get_stacks(containers)
    stacks2 = get_stacks(containers)
    #print(stacks1)
    sts = move_containers(movements,stacks1)
    #print(stacks1)
    sts2 = move_containers_part2(movements,stacks2)
    print("Solution Part1 -> ", end="")
    for s in sts:
        print(s.pop(), end="")
    print("\n")
    print("Solution Part2 -> ", end="")
    for s in sts2:
        print(s.pop(), end="")
    print("\n")
