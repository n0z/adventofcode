with open("input","r") as f:
    program=f.read().split(",")

def compute(program:list, v1:str, v2:str) -> str:
    program=program.copy()
    program[1]=v1; program[2]=v2
    i=0
    res_pos=0
    while i<len(program):
        opCode=program[i]
        if opCode=="99": break
        else:
            a=program[int(program[i+1])]; b=program[int(program[i+2])]
            if opCode=="1":
                res_pos=int(program[i+3])
                program[int(program[i+3])]=str(int(a)+int(b))
            elif opCode=="2":
                res_pos=int(program[i+3])
                program[int(program[i+3])]=str(int(a)*int(b))
            else: print("ERROR"); break
        i+=4
    return program[res_pos]

def calculate_pair_inputs(program) -> int and int:
    for i in range(0,100):
        for j in range(0,100):
            if compute(program,str(i),str(j)) == "19690720": return i,j

print("Part1 -- What value is left at position 0 after the program halts? "
      +str(compute(program,12,2)))
a,b=calculate_pair_inputs(program)
print("Part2 -- What is 100*noun+verb? "+str(100*a+b))
