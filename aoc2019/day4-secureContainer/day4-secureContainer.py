
inp='256310-732736'.split('-')
passwords=[str(number) for number in range(int(inp[0]),int(inp[1])+1)]
passwords=[password for password in passwords if ''.join(sorted(password))==password]

valids_p1=0; valids_p2=0
for password in passwords:
    i=1; adjacents=False; groups={}
    while i<len(password):
        if password[i]==password[i-1]:
            if groups.get(password[i-1])==None: groups[password[i-1]]=2; i+=1
            else: groups[password[i-1]]+=1; i+=1
            adjacents=True 
        else: i+=1
        
    if adjacents:
        valids_p1+=1
        if len(groups)==1 and list(groups.values())[0]<=2:valids_p2+=1
        if len(groups)>1 and 2 in list(groups.values()): valids_p2+=1
print("Part1 -- How many different passwords within the range given in \
your puzzle input meet these criteria? "+str(valids_p1))
print("Part2 -- How many different passwords within the range given in \
your puzzle input meet all of the criteria? "+str(valids_p2))
