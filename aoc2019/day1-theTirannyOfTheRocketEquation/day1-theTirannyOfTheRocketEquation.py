import math

with open("input","r") as f:
    modules=f.read().split()

def calculate_fuel_requeriments(modules: list) -> int and int: 
    sum_part1=sum([math.floor(int(module)/3)-2 for module in modules])
    sum_part2=0
    for module in modules:
        fuel=math.floor(int(module)/3)-2; total_fuel=fuel
        while fuel>0:
            fuel=math.floor(int(fuel)/3)-2
            if fuel>0:total_fuel+=fuel
        sum_part2+=total_fuel
    return sum_part1,sum_part2

p1,p2=calculate_fuel_requeriments(modules)
print("Part1 -- What is the sum of the fuel requirements? "+str(p1))
print("Part2 -- What is the sum of the fuel requirements\
when also taking into account the mass of the added fuel? "+str(p2))
