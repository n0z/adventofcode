
def move(instruction:str,x:int,y:int) -> list:
    points=[]
    direction=instruction[:1]; num=int(instruction[1:])
    if direction=='R':
        for i in range(y+1,y+num+1): y+=1;points.append((x,y))
    elif direction=='L':
        for i in range(y+num,y,-1): y-=1; points.append((x,y))
    elif direction=='U':
        for i in range(x+num,x,-1): x-=1; points.append((x,y))
    elif direction=='D':
        for i in range(x+1,x+num+1): x+=1; points.append((x,y))
    return points


with open("input","r") as f:
    path1,path2=f.read().split("\n")
path1=path1.split(','); path2=path2.split(',')
path1_points={}; path2_points={}

orig_point=(0,0)
x,y=orig_point[0],orig_point[1]
steps=0
for instruction in path1:
    points=move(instruction,x,y)
    x,y=points[-1][0],points[-1][1]
    for point in points: steps+=1;path1_points[point]=steps 

x,y=orig_point[0],orig_point[1]
steps=0
for instruction in path2:
    points=move(instruction,x,y)
    x,y=points[-1][0],points[-1][1]
    for point in points: steps+=1;path2_points[point]=steps 

croses=set(path1_points)&set(path2_points)
closet_intersection=[]
min_distance=9999;steps=9999999
for point in croses:
    if abs(point[0])+abs(point[1]) < min_distance:
           min_distance=abs(point[0])+abs(point[1])

for point in croses:
    if path1_points[point]+path2_points[point]<steps:
        steps=path1_points[point]+path2_points[point]

print("Part1 -- What is the Manhattan distance from the central port \
to the closest intersection? "+str(min_distance))
print("Part2 -- What is the fewest combined steps the wires must take \
to reach an intersection? " +str(steps))
