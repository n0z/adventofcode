def parse_grid(lines:list) -> list:
    grid=[]
    for line in lines:
        c=[]
        for height in line:
            c.append(height)
        grid.append(c)
    return grid

def check_low_point(row:int,col:int,grid:list) -> bool:
    if row-1>=0 and grid[row][col]>=grid[row-1][col]: return False
    if row+1<len(grid) and grid[row][col]>=grid[row+1][col]: return False
    if col-1>=0 and grid[row][col]>=grid[row][col-1]: return False
    if col+1<len(grid[0]) and grid[row][col]>=grid[row][col+1]: return False
    return True

def check_high_points(row:int,col:int,grid:list,visited:set):
    size=0
    
    if ((row,col)) in visited or grid[row][col]=='9': return size
    size+=1
    visited.add((row,col))
    if row-1>0: size+=check_high_points(row-1,col,grid,visited)
    if row+1<len(grid): size+=check_high_points(row+1,col,grid,visited)
    if col-1>=0: size+=check_high_points(row,col-1,grid,visited)
    if col+1<len(grid[0]): size+=check_high_points(row,col+1,grid,visited)
    return size
    
def get_low_points(grid: list) -> list:
    values=[]
    pos=[]
    for r in range(0,len(grid)):
        for c in range(0,len(grid[0])):
            if check_low_point(r,c,grid):
                values.append(int(grid[r][c]))
                pos.append((r,c))
    return values,pos

def get_basins_sizes(low_points:list,grid:list) -> list:
    basins_sizes=[]
    for point in low_points:
        visited=set()
        basins_sizes.append(check_high_points(point[0],point[1],grid,visited))
    return basins_sizes
       

with open("input","r") as f:
    lines=f.read().split()

grid=parse_grid(lines)
values,pos=get_low_points(grid)

total_p1=sum([value+1 for value in values])
print("Part1 -- What is the sum of the risk levels of all low points \
on your heightmap? "+str(total_p1))
basins_sizes=get_basins_sizes(pos,grid); basins_sizes.sort()
res=1
for basin in basins_sizes[-3:]: res*=basin
print("Part2 -- What do you get if you multiply together the sizes of \
the three largest basins? "+str(res))
