with open("input","r") as f:
    fuels=f.read().split(",")

size=len(fuels)
totals_part1=[]
totals_part2=[]
for x in range(0,size):
    total_p1=0
    total_p2=0
    n=0
    for pos in fuels:
        total_p1+=abs(int(pos)-int(x))
        p2=abs(int(pos)-int(x));n=p2-1
        total_p2+=p2+(n*(n+1)//2)
    totals_part1.append(total_p1)
    totals_part2.append(total_p2)
totals_part1.sort()
totals_part2.sort()
print(totals_part1[0])
print(totals_part2[0])
