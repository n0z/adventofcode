with open("input","r") as f:
    lines=f.read().split("\n")

def decode_numbers(signals: list) -> list:
    patterns={}
    while len(signals)!=6:
        for i,signal in enumerate(signals):
            if len(signal)==2: patterns[1]=''.join(sorted(signal));signals.remove(signal)
            elif len(signal)==3: patterns[7]=''.join(sorted(signal));signals.remove(signal)
            elif len(signal)==4: patterns[4]=''.join(sorted(signal));signals.remove(signal)
            elif len(signal)==7: patterns[8]=''.join(sorted(signal));signals.remove(signal) 
    while len(signals)!=4:
        for signal in signals:
            if len(signal)==5 and set(patterns[7]).issubset(set(signal)):
                patterns[3]=''.join(sorted(signal)); signals.remove(signal)
            if len(signal)==6 and set(patterns[4]).issubset(set(signal)):
                patterns[9]=''.join(sorted(signal)); signals.remove(signal)
    for signal in signals:
        if len(signal)==6 and set(patterns[7]).issubset(set(signal)):
            patterns[0]=''.join(sorted(signal)); signals.remove(signal)
    while len(signals)!=0:
        for signal in signals:
            if len(signal)==6: patterns[6]=''.join(sorted(signal)); signals.remove(signal)
            if len(signal)==5:
                if set(signal).issubset(set(patterns[9])):
                    patterns[5]=''.join(sorted(signal)); signals.remove(signal)
                else: patterns[2]=''.join(sorted(signal)); signals.remove(signal)
    signal_patterns={}
    for k,v in patterns.items(): signal_patterns[v]=k 
    return signal_patterns


total_p1=0
total_p2=0
for line in lines:
    signals,output=line.split("|")
    signals=signals.strip().split(); output=output.strip().split()
    num=""
    patterns=decode_numbers(signals)
    for out in output:
        size=len(out)
        if size in [2,4,3,7]: total_p1+=1
        out=''.join(sorted(out))
        num+=str(patterns[out])
    total_p2+=int(num)

print("Part1 --In the output values, how many times do digits 1, 4, 7, or 8 appear? "
      +str(total_p1))
print("What do you get if you add up all of the output values? "+str(total_p2))

        
