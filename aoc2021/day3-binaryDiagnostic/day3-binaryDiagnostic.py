from copy import deepcopy


def count_0s_and_1s(data: list, col: int):
    numOnes, numZeros = 0,0
    for d in data:
        if d[col] =='1': numOnes+=1
        else: numZeros+=1

    return numOnes, numZeros

def calculate_power_consumption(lines: list):
    gammaRate=""; epsilonRate=""; size=len(lines[0])

    for i in range(0,size):
        numOnes, numZeros = count_0s_and_1s(lines,i)
        if numOnes>numZeros: gammaRate+='1'; epsilonRate+='0'; 
        elif numZeros>numOnes: gammaRate+='0'; epsilonRate+='1'; 
        
    return (int(gammaRate,2)*int(epsilonRate,2))

def calculate_life_support_rating(lines: list):
    oxygenData=deepcopy(lines)
    co2Data=deepcopy(lines)

    i=0
    while len(oxygenData)>1:
        numOnes, numZeros = count_0s_and_1s(oxygenData,i)
        if numOnes>=numZeros: oxygenData=[number for number in oxygenData if number[i] == '1']
        else: oxygenData=[number for number in oxygenData if number[i] == '0']
        i+=1

    i=0
    while len(co2Data)>1:
        numOnes, numZeros = count_0s_and_1s(co2Data,i)
        if numZeros<=numOnes: co2Data=[number for number in co2Data if number[i] =='0']
        else: co2Data=[number for number in co2Data if number[i] == '1']
        i+=1

    return int(oxygenData[0],2)*int(co2Data[0],2)


with open("input","r") as f:
    lines = f.read().split("\n")

print("Part1 -- What is the power consumption of the submarine? "
      + str(calculate_power_consumption(lines)))
print("Part2 -- What is hte life supoort rating of the submarine? "
      + str(calculate_life_support_rating(lines)))
