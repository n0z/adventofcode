def get_xys(coords: list) -> int and int and int and int:
    x_ini,y_ini=int(coords[0][0]),int(coords[0][1])
    x_end,y_end=int(coords[1][0]),int(coords[1][1])
    return x_ini,y_ini,x_end,y_end

def grid_width(lines:list) -> int:
    x_max,y_max=0,0
    for coord in lines:
        x_ini,y_ini,x_end,y_end=get_xys(coord)
        if x_ini > x_max: x_max=x_ini
        if x_end > x_max: x_max=x_end
        if y_ini > y_max: y_max=y_ini
        if y_end > y_max: y_max=y_end
    if x_max>y_max: return x_max+1
    else: return y_max+1

"""
 Separate horizontal and vertical lines for part1
 from diagonal lines (part 2)
"""
def separate_lines(lines: list) -> list and list:
    hv_lines=[]; diagonal_lines=[]
    #hv_lines=[if coord[0][0]==cord[1][0] or coord[0][1]==cord[1][1]
    #          for coord in lines]
    for coord in lines:
        if coord[0][0]==coord[1][0] or coord[0][1]==coord[1][1]:
            hv_lines.append(coord)
        else: diagonal_lines.append(coord) #hv_lines.append(coord)
    return hv_lines,diagonal_lines
    
def create_empty_grid(size:int) -> list:
    grid=[["." for col in range(0,size)]for row in range(0,size)]
    return grid

def draw_lines(grid:list, lines:list) -> list:
    for coord in lines:
        x_ini,y_ini,x_end,y_end=get_xys(coord)
        if x_ini==x_end: #same row so we have to draw column
            if y_ini >= y_end: ini=y_end; end=y_ini
            else: ini=y_ini; end=y_end
            
            while ini<=end:
                if grid[ini][x_ini]==".": grid[ini][x_ini]="1"
                else: grid[ini][x_ini]=str(int(grid[ini][x_ini])+1)
                ini+=1
        elif y_ini==y_end: #draw row
            if x_ini >= x_end: ini=x_end; end=x_ini
            else: ini=x_ini; end=x_end
            while ini<=end:
                if grid[y_ini][ini]==".": grid[y_ini][ini]="1"
                else: grid[y_ini][ini]=str(int(grid[y_ini][ini])+1)
                ini+=1
    return grid

def draw_diagonal_lines(grid:list, lines:list) -> list:
    #print("####### draw diagonal lines #######")
    for coord in lines:
        x_ini,y_ini,x_end,y_end=get_xys(coord)
        while x_ini!=x_end:
            elem=grid[y_ini][x_ini]
            if grid[y_ini][x_ini]==".": grid[y_ini][x_ini]="1"
            else: grid[y_ini][x_ini]=str(int(grid[y_ini][x_ini])+1)
            (x_ini:=x_ini-1 if x_ini>x_end else (x_ini:=x_ini+1))
            (y_ini:=y_ini-1 if y_ini>y_end else (y_ini:=+y_ini+1))
        if grid[y_ini][x_ini]==".": grid[y_ini][x_ini]="1"
        else: grid[y_ini][x_ini]=str(int(grid[y_ini][x_ini])+1)
        
    return grid

def count_overlap_points(grid:list) -> int:
    cont=0
    size=len(grid)
    for row in range(0,size):
        for col in range(0,size):
            if grid[row][col].isdigit() and int(grid[row][col])>1: cont+=1
    return cont


with open("input","r") as f:
    lines = f.read().split("\n")

lines=[[coord.split(",") for coord in line.replace(" ","").split("->")]
       for line in lines]
hv_lines,diagonal_lines=separate_lines(lines)
grid=create_empty_grid(grid_width(lines))
grid=draw_lines(grid,hv_lines)
print("Part1-- How many points do at least two lines overlap? "
      +str(count_overlap_points(grid)))
grid=draw_diagonal_lines(grid,diagonal_lines)
print("Part2-- How many points do at least two lines overlap? "
      +str(count_overlap_points(grid)))
    
