with open("input","r") as f:
    input = f.read().split("\n\n")


numbers=input[0].split(",")
boards=[board.split("\n") for board in input[1:]]
bingoBoards=[]

for board in boards:
    board.append('a a a a a')
    b=[]
    for i,line in enumerate(board):
        if i!=5: line+=" a"
        b.append(line.split())
    bingoBoards.append(b)


def play(bingoBoards: list,startnumber):
    winner=False
    winnerBoard=[]
    bn=-1
    while not winner:
        iNum=0
        if startnumber != -1: iNum=startnumber 
        while iNum < len(numbers):
        #for iNum in range(0,len(numbers)):        
            for bn, board in enumerate(bingoBoards):
                #mark numbers if appear in board
                for row in range(0,len(board)-1):
                    if board[row].count(numbers[iNum]) == 1:
                        ind=board[row].index(numbers[iNum])
                        #ind=iNum
                        board[row][ind] = "#"; board[row][5]=chr(ord(board[row][5])+1)
                        board[5][ind]=chr(ord(board[5][ind])+1)
                #check rows
                for r in range(0,len(board)-1):
                    if board[r][5] == "f":
                        winner=True; winnerBoard=board;
                        return winnerBoard,numbers[iNum],bn,iNum

                #check columns
                for c in range(0,5):
                    if board[5][c] == "f":
                        winner=True; winnerBoard=board;
                        return winnerBoard, numbers[iNum], bn,iNum
            iNum+=1
      
def calculate_score(winnerboard, number):
    suma=0
    for row in range(0,len(winnerboard)-1):
        for col in range(0,len(winnerboard[0])-1):
            if winnerboard[row][col].isdigit():
                suma+=int(winnerboard[row][col])
    return (suma*int(number))


def last_board_winner(bingoBoards):
    iNum=0
    #Find the last 
    while len(bingoBoards) > 1:
        winnerBoard,number,bn,iNum=play(bingoBoards,iNum)
        bingoBoards.pop(bn)
    #Play to the end
    winnerBoard,number,bn,iNum=play(bingoBoards,iNum)
    return winnerBoard,number


winnerBoard,number,bn,iNum=play(bingoBoards,-1)
print("Part1 -- What will your final score be if you choose that board? "
      +str(calculate_score(winnerBoard,number)))
lastWinnerBoard,number = last_board_winner(bingoBoards)
print("Part2 -- What will be the final score of the last winner board? "
      +str(calculate_score(lastWinnerBoard,number)))
