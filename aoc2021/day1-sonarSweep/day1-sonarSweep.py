with open("input","r") as f:
    depths = f.read().split("\n")
def count_increased(depths: list):
    return sum([int(depths[i])>int(depths[i-1])
                for i in range(1,len(depths))])

part1=count_increased(depths)
print("Part1 -- How many measurements are larger than the previous measurement? "+str(part1))

wdepths=[int(depths[i])+int(depths[i+1])+int(depths[i+2]) for i in range(0,len(depths)) if i+2<len(depths)]

print("Part2 -- How many sums are larger than the previous sum? "+ str(count_increased(wdepths)))
