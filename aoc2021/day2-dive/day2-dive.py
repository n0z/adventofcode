with open("input","r") as f:
    commands = f.read().split("\n")

commands=[command.split() for command in commands ]
subPos=[0,0]
subPos2=[0,0,0] #[horizontal,depth,aim]
for command in commands:
    direction,value=command
    if direction=="down": subPos[1]+=int(value); subPos2[2]+=int(value)
    elif direction=="up": subPos[1]-=int(value); subPos2[2]-=int(value)
    else: subPos[0]+=int(value); subPos2[0]+=int(value); subPos2[1]+=(subPos2[2]*int(value))

print("What do you get if you multiply your final horizontal position by your final depth? "+str(subPos[0]*subPos[1]))
print("What do you get if you multiply your final horizontal position by your final depth? "+str(subPos2[0]*subPos2[1]))
    
