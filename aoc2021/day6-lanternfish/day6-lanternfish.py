"""
 Count number of lanternfish in x days
"""
def number_of_lanternfish(days: int, ages: list) -> int:
    fish_times=[int(age) for age in ages]
    lanternfishes=[0 for i in range(0,9)]
    for fish in fish_times: lanternfishes[fish]+=1
    for day in range(0,days):
        lanternfishes.append(lanternfishes.pop(0))
        lanternfishes[6]+=lanternfishes[-1]

    return sum(lanternfishes)

with open("input","r") as f:
    ages = f.read().split(",")
ages=[int(age) for age in ages]

print("Part1 -- How many lanternfish would there be after 80 days? "
      +str(number_of_lanternfish(80,ages)))

print("Part2 -- How many lanternfish would there be after 256 days? "
      +str(number_of_lanternfish(256,ages)))
