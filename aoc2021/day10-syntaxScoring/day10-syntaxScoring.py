with open("input","r") as f:
    lines= f.read().split("\n")

points={')':3, ']':57, '}':1197, '>':25137}
error_score=0
incomplete_lines=[]
for line in lines:
    stack=[]
    for c in line:
        if c in ['(','[','{','<']: stack.append(c)
        elif c==')' and stack[-1]=='(': stack.pop()
        elif c==']' and stack[-1]=='[': stack.pop()
        elif c=='}' and stack[-1]=='{': stack.pop()
        elif c=='>' and stack[-1]=='<': stack.pop() 
        else: stack.append(c);  break
        
    if len(stack)!=0 and stack[-1] in [')',']','}','>']:
        error_score+=points[stack[-1]]
    else:
        #Get incomplete lines
        incomplete_lines.append(stack)
print("Part1 -- What is the total syntax error score for those errors? "
      +str(error_score))

points_p2={'(':1,'[':2,'{':3,'<':4}
completion_scores=[]
for stack in incomplete_lines:
    score=0
    for c in reversed(stack):
        score=score*5+points_p2[c]
    completion_scores.append(score) 
completion_scores.sort()
print("Part2 -- What is the middle score? "
      +str(completion_scores[len(completion_scores)//2]))
