import hashlib

secret_key="iwrupvqb"

def calculate_decimal(secret_key:str, zeros:int) -> str:
    decimal=""
    for number in range(100000,10000000):
        sk = secret_key+str(number)
        h=hashlib.md5(bytes(sk,'utf-8')).hexdigest()
        i=0
        while i<zeros:
            if h[i]=="0": i+=1
            else: break
        if i==zeros: decimal=str(number); break
    return decimal

print("Part1-- Find decimal that produces a MD5 hash that\
start with five zeros: " +calculate_decimal(secret_key,5))

print("Part2-- Find decimal that produces a MD5 hash that\
start with six zeros: " +calculate_decimal(secret_key,6))
