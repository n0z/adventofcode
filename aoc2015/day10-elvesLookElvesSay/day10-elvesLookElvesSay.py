
def calculate_conway_sequence(secuence:str, times: int) -> int:
    for n in range(0,times):
        #print(secuence)
        new=""
        value=secuence[0]
        num=0
        #print("value->"+value)
        #print("num->"+str(num))
        for i in range(0,len(secuence)):
            if secuence[i]==value: num+=1
            else:
                new+=str(num)+value
                value=secuence[i]
                num=1
        new+=str(num)+value
        secuence=new
    return len(new)

print("Part1 -- What is the lenght of the secuence if we apply\
the process 40 times? "+str(calculate_conway_sequence("1113122113",40)))
print("Part2 -- What is the lenght of the secuence if we apply\
the process 50 times? "+str(calculate_conway_sequence("1113122113",50)))
