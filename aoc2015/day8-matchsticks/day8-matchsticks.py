def encode(lines:list) -> list:
    encoded_lines=[]
    for line in lines:
        i=0; new=""
        while i<len(line):
            if line[i]=='"': new+='\\"'; i+=1
            elif line[i]=='\\': new+='\\\\';  i+=1
            else:  new+=line[i]; i+=1
        new='"'+new+'"'; encoded_lines.append(new)
    return encoded_lines
        
def count(lines:list) -> int and int:
    total_characters=0
    total_memory=0
    for line in lines:
        characters=len(line); memory=0
        i=0
        while i<len(line):
            if line[i]=='"': i+=1
            elif line[i]=='\\' and line[i+1]=='"': memory+=1; i+=2
            elif line[i]=='\\' and line[i+1]=="\\": memory+=1; i+=2
            elif line[i]=='\\' and line[i+1]=="x": memory+=1; i+=4
            else: memory+=1; i+=1
            
        total_characters+=characters
        total_memory+=memory
    return total_characters,total_memory


with open("input","r") as f:
    lines=f.read().split("\n")

tchars,tmemory=count(lines)
print("Part1 -- What is the number of characters of code for string\
literals minus the number of characters in memory for the values of\
the strings in total for the entire file? "+str(tchars-tmemory))

tchars_part2,tmemory=count(encode(lines))
print("Part2 -- Find the total number of characters to represent the\
newly encoded strings minus the number of characters of code in each\
original string literal. "+str(tchars_part2-tchars))

