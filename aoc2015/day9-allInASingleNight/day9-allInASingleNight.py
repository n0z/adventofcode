from itertools import permutations

def parse(lines: list) -> dict and list:
    d={}; all_cities=set()
    for line in lines:
        cities,distance=line.split("="); distance=distance.strip()
        origin_citie,dest_citie=cities.split("to")
        origin_citie=origin_citie.strip(); dest_citie=dest_citie.strip()
        all_cities.add(origin_citie); all_cities.add(dest_citie)
        d[origin_citie+dest_citie]=int(distance)
        d[dest_citie+origin_citie]=int(distance)
    return d,all_cities

def calculate_distances(d: dict, all_cities: list) -> int and int:
    min_distance=9999; max_distance=0
    for cities in permutations(all_cities):
        distance=0
        for i in range(0,len(cities)-1): distance+=d.get(cities[i]+cities[i+1])
        if distance<min_distance: min_distance=distance 
        if distance>max_distance: max_distance=distance
    return min_distance,max_distance


with open("input","r") as f:
   lines= f.read().split("\n")

routes,all_cities=parse(lines)
shortest_route,longest_route=calculate_distances(routes,all_cities)
print("Part1 -- What is the distance of the shortest route? "
      +str(shortest_route))
print("Part2 -- What is the distance of the longest route? "
      +str(longest_route))
      
       
