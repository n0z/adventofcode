with open("input","r") as f:
    instructions = f.read()

floor=0
position_basement=-1
founded=False
for index,instruction in enumerate(instructions):
    if instruction=="(": floor+=1
    else: floor-=1
    if floor==-1 and not founded:
        position_basement=index+1; founded=True

print("To what floor do the instructions take Santa? "+str(floor))
print("What is the position of the character that causes\
 Santa to first enter the basement? "+str(position_basement))
