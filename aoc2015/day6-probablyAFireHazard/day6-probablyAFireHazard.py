import copy
with open("input","r") as f:
    instructions=f.read().split("\n")

grid_part1=[]
for r in range(0,1000):
    l=[]
    for c in range(0,1000):
        l.append(0)
    grid_part1.append(l)
grid_part2=copy.deepcopy(grid_part1)

for inst in instructions:
    instruction=inst.split()
    flag_on=False; flag_off=False;flag_toggle=False
    if instruction[0]=="turn":
        if instruction[1]=="on": flag_on=True 
        else: flag_off=True
        x_ini,y_ini=instruction[2].split(",")
        x_fin,y_fin=instruction[4].split(",")
    else:
        flag_toggle=True
        x_ini,y_ini=instruction[1].split(",")
        x_fin,y_fin=instruction[3].split(",")
            
    for row in range(int(x_ini),int(x_fin)+1):
        for col in range(int(y_ini),int(y_fin)+1):
            if flag_on:
                grid_part1[row][col]=1
                grid_part2[row][col]+=1
            elif flag_off:
                grid_part1[row][col]=0
                if grid_part2[row][col]>0: grid_part2[row][col]-=1
            elif flag_toggle:
                if grid_part1[row][col]==1: grid_part1[row][col]=0
                else: grid_part1[row][col] = 1
                grid_part2[row][col]+=2

part1=0
part2=0
for row in range(0,1000):
    for col in range(0,1000):
        if grid_part1[row][col]==1: part1+=1
        part2+=grid_part2[row][col]

print("Part1 -- How many lights are lit? "+str(part1))
print("Part2 -- What is the total brightness of all lights combined? "+str(part2))
