with open("input","r") as f:
    movements = f.read()
"""
    Set of visited houses with the initial
    house visited in it
"""
def move(move,x,y) -> tuple:
    if move=="^": x+=1 
    elif move=="v": x-=1
    elif move==">": y+=1
    elif move=="<": y-=1
    return (x,y)

visited_houses=set((0,0)); x,y=0,0
santa_and_robot_visited=set(); sx,sy=0,0; rx,ry=0,0
for i in range(len(movements)):
    x,y=move(movements[i],x,y)
    visited_houses.add((x,y))
    if i%2==0:
        sx,sy=move(movements[i],sx,sy)
        santa_and_robot_visited.add((sx,sy))
    else:
        rx,ry=move(movements[i],rx,ry)
        santa_and_robot_visited.add((rx,ry))

print("Part1 -- How many houses receive at least one present? "
      +str(len(visited_houses)))
print("Part2 -- how many houses receive at least one present? "
      +str(len(santa_and_robot_visited)))
