
def parse(circuit: list) -> dict and int:
    d={}; num_digits=0
    for c in circuit:
        l,r=c.strip().split("->")
        l=l.strip();r=r.strip()
        if l.isdigit(): d[r]=[l,True]; num_digits+=1
        else: d[r]=[l,False]
    return d,num_digits

def operate(a:str, b:str, op:str,instructions: dict) -> str or None:
    if not a.isdigit():
        if instructions[a][0].isdigit(): a=instructions[a][0]
    if not b.isdigit():
        if instructions[b][0].isdigit(): b=instructions[b][0]
    if a.isdigit() and b.isdigit():
        a=int(a); b=int(b)
        if op=="AND": return str(a&b)
        elif op=="OR": return str(a|b)
        elif op=="LSHIFT": return str(a<<b)
        elif op=="RSHIFT": return str(a>>b)
        elif op=="NOT": return str(~a+(1<<16))
    else: return None

def emulate(circuit: dict, num_digits:int) -> str:
    circuit=circuit.copy()
    size_circuit=len(circuit)
    while num_digits!=size_circuit-1:
        for k,v in circuit.items():
            line=v[0].split()
            res=None
            if len(line)==1:
                if circuit.get(line[0][0])!=None: circuit[k]=circuit[line[0]]
            if len(line)==2:
                res=operate(line[1],"0","NOT",circuit)
            elif len(line)==3:
                a=line[0]; op=line[1]; b=line[2]
                res=operate(a,b,op,circuit)
            if res!=None: circuit[k]=[res,True]; num_digits+=1
    return circuit["a"][0]
    
with open("input","r") as f:
    circuit=f.read().split("\n")

circuit,num_digits=parse(circuit)
res=emulate(circuit,num_digits)
print("Part1 -- What signal is ultimately provided to wire a? "
      +res)
circuit["b"][0]=res
print("Part2 -- What new signal is ultimately provided to wire a? "
      +emulate(circuit,num_digits))
