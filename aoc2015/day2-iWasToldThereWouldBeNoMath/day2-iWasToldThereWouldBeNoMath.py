with open("input","r") as f:
    dimensions=f.read().split()

paper_size=0
ribbon_size=0

for dimension in dimensions:
    ds=dimension.split("x")
    l=int(ds[0]); w=int(ds[1]); h=int(ds[2])
    ds=[l,w,h]; ds.sort(); extra=ds[0]*ds[1]
    paper_size+=((2*l*w) + (2*w*h) + (2*h*l)+extra)
    ribbon_size+=((2*ds[0])+(2*ds[1])+(l*w*h))

print("Part1 -- How many total square feet of wrapping paper should\
they order? "+str(paper_size))
print("Part2 -- How many total feet of ribbon should they order? "
      + str(ribbon_size))
