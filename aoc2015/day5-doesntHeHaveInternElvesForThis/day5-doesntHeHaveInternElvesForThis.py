with open("input","r") as f:
    strings=f.read().split()

vowels=["a","e","i","o","u"]
prohibited_strs=["ab","cd","pq","xy"]
nice_strings=0
for string in strings:
    num_vowels=0; prohibited=False; double_letter=False
    size=len(string)
    for i in range(0,size):
        if i+1 < size and string[i]+string[i+1] in prohibited_strs:
            prohibited=True
            break   
        if string[i] in vowels: num_vowels+=1
        if i+1 < size and string[i]==string[i+1]: double_letter=True

    if not prohibited and double_letter and num_vowels>=3: nice_strings+=1

print("Part1->"+str(nice_strings))

nice_strings=0
for string in strings:
    size=len(string)
    double_letter=False; num_aba_pattern=0;
    for i in range(0,size):
       if i+1<size:
            pair=(string[i]+string[i+1])
            cont=string.count(pair)
            if cont>=2:
                if pair[0]==pair[1]:
                    indices=[i for i in range(size)
                             if string.startswith(pair,i)]
                    for indice in indices:
                        if indice+2<size:
                            if string[indice+1]!=string[indice+2]:
                                double_letter=True
                elif pair[0]!=pair[1]: double_letter=True

            if i+2<size and string[i]==string[i+2]: num_aba_pattern+=1
                        
    if double_letter and num_aba_pattern>=1: nice_strings+=1

print("Part2->"+str(nice_strings))

        
