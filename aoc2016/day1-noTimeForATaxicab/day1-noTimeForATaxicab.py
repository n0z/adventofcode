with open("input","r") as f:
    directions=f.read().replace(' ','').split(",")

#directions=['R8','R4','R4','R8']
R={'N':'E','E':'S','S':'W','W':'N'}
L={'N':'W','E':'N','S':'E','W':'S'}
actual_dir='N'; point=[0,0]
visited=[]
point_twice=""
found_twice=False
for direction in directions:
    direc=direction[:1]
    num=int(direction[1:])
    previous_point=point.copy()
    if direc=="R": actual_dir=R[actual_dir]
    else: actual_dir=L[actual_dir]
    if actual_dir=='N': point[0]+=num
    elif actual_dir=='E': point[1]+=num
    elif actual_dir=='S': point[0]-=num
    elif actual_dir=='W': point[1]-=num

    if not found_twice:
        x=previous_point[0]-point[0]; y=previous_point[1]-point[1]
        if x<0:
            for i in range(previous_point[0],point[0]):
                if ((i,previous_point[1])) in visited:
                    point_twice=(i,previous_point[1]);found_twice=True
                else: visited.append((i,previous_point[1]))
        elif x>0:
            for i in range(previous_point[0],point[0],-1):
                if ((i,previous_point[1])) in visited:
                    point_twice=(i,previous_point[1]); found_twice=True
                else: visited.append((i,previous_point[1]))
        if y<0:
            for i in range(previous_point[1],point[1]):
                if ((previous_point[0],i)) in visited:
                    point_twice=(previous_point[0],i); found_twice=True
                else: visited.append((previous_point[0],i))
        elif y>0:
            for i in range(previous_point[1],point[1],-1):
                if ((previous_point[0],i)) in visited:
                    point_twice=(previous_point[0],i); found_twice=True
                else: visited.append((previous_point[0],i))
        

print("Part1 -- How many blocks away is Easter Bunny HQ? "
      +str(abs(0-point[0])+abs(0-point[1])))
print("Part2 -- How many blocks away is the first location you visit twice? "
      +str(abs(0-point_twice[0])+abs(0-point_twice[1])))


