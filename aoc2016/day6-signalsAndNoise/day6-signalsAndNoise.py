with open("input","r") as f:
    messages=f.read().split("\n")
messages=[[c for c in message] for message in messages]

corrected_message=""
original_message=""
for c in range(0,len(messages[0])):
    characters={}
    for message in messages:
        if characters.get(message[c])==None: characters[message[c]]=1
        else: characters[message[c]]+=1
    characters=dict(sorted(characters.items(),key=lambda a:a[1],reverse=True))
    corrected_message+=list(characters.keys())[0]
    original_message+=list(characters.keys())[-1]

print("Part1 -- What is the error-corrected version of the message being sent? "
      +corrected_message)
print("Part2 -- What is the original message that Santa is trying to send? "
      +original_message)
