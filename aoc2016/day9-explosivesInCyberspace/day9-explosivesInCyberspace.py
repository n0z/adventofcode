import time
def decompress(string:str) -> str:
    #for line in lines:
    #print("line->"+str(line))
    line=string
    i=0
    string=""
    while i<len(line):
        if line[i]=='(':
            i+=1
            specs=""
            while line[i+1]!=')': specs+=line[i];i+=1
            specs+=line[i]
            #print("specs->"+str(specs))
            num_letters,times=specs.split('x')
            i+=2#to the first letter after (AxB)X
            letters=""
            while len(letters)<int(num_letters): letters+=line[i]; i+=1
            string+=letters*int(times)
            #print("i des letters->"+str(i))
            if i<len(line) and line[i]!='(' and line[i]!=')':
                string+=line[i]
            else: i-=1
            #print("string comprobacion multiple->"+str(string))
        else: string+=line[i]#; print("str else->"+str(string))
        i+=1
        #print(i)
    return string


with open("input","r") as f:
    lines=f.read().strip().split("\n")

decompressed_length_p1=0
decompressed_length_p2=0
for line in lines:

    string=decompress(line)
    decompressed_length_p1+=len(string)

    #while string.find('(') != -1:
    #    string=decompress(string)
        #print("a")
    #decompressed_length_p2=len(string)
    #print("len->"+str(len(string)))
    
print("Part1 -- What is the decompressed length of the file? "
      +str(decompressed_length_p1))
#print("Part2 -- What is the decompressed length of the file using \
#this improved format? "+str(decompressed_length_p2))
