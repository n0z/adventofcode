with open("input","r") as f:
    triangles=f.read().split("\n")
possibles=0
new_triangles=[]
for triangle in triangles:
    triangle=triangle.split();
    new_triangles.append(triangle)
    a=int(triangle[0]); b=int(triangle[1]); c=int(triangle[2])
    #valid=False
    if a+b>c and b+c>a and c+a>b: possibles+=1
    #if valid: possibles+=1

print("Part1 -- How many of the listed triangles are possible? "
      +str(possibles))
triangles=new_triangles
possibles=0
for col in range(0,3):
    for i in range(0,len(triangles),3):
        a=int(triangles[i][col])
        b=int(triangles[i+1][col])
        c=int(triangles[i+2][col])
        if a+b>c and b+c>a and c+a>b: possibles+=1
        #i+=3

print("Part2 -- Instead reading by columns, how many of the listed triangles are possible? "+str(possibles))
