from collections import Counter

with open("input","r") as f:
    rooms=f.read().split("\n")

def get_check(letters:dict)->str:
    check=[]
    same_value=[]
    while len(check)!=5:
        if len(letters)>0:
            item=next(iter(letters))
            value=letters.get(item)
        for k,v in letters.items():
            if value==v: same_value.append(k)
            else: break
        for elem in same_value: letters.pop(elem)
        same_value.sort()
        rest=5-len(check)
        if len(same_value)>rest: check+=same_value[:rest]
        else: check+=same_value
        same_value=[]
    return ''.join(check) 

def decipher(data:str, key:str) -> str:
    plaintext=""
    for c in data:
        #print("c->"+str(c))
        if c=='-': plaintext+=' '
        else:
            letter_ascii=ord(c)+(abs(26-int(key))%26)
            if letter_ascii>ord('z'):letter_ascii-=26
            plaintext+=chr(letter_ascii)
    return plaintext

ids_sum=0
room_id_secret=""
for room in rooms:
    letters=''.join(room.split("-")[:-1])
    data=room.split("[")[0];data=data[:-4]
    room_id,checksum=room.split("-")[-1].split("[")
    checksum=checksum[:-1]
    counted=Counter(letters) 
    counted=dict(sorted(counted.items(),key=lambda a: a[1], reverse=True))
    check=get_check(counted)
    if check==checksum: ids_sum+=int(room_id)
    plaintext=decipher(data,room_id)
    if "object storage" in plaintext:
        room_id_secret+=room_id
    
print("Part1 -- What is the sum of the sector IDs of the real rooms? "
      +str(ids_sum))
print("Part2 -- What is the sector ID of the room where North Pole \
objects are stored? "+str(room_id_secret))
