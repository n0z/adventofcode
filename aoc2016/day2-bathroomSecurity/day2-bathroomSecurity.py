with open("input","r") as f:
   instructions = f.read().split("\n")

keypad1=[['1','2','3'],
        ['4','5','6'],
        ['7','8','9']]
keypad2=[['.','.','1','.','.'],
         ['.','2','3','4','.'],
         ['5','6','7','8','9'],
         ['.','A','B','C','.'],
         ['.','.','D','.','.']]
def get_initial_position(keypad:list)->list:
    for r in range(0,len(keypad)):
        for c in range(0,len(keypad[r])):
            if keypad[r][c]=='5': return [r,c]

def get_bathroom_code(instructions: list, keypad: list) -> str:
    bathroom_code=""
    position=get_initial_position(keypad)
    for instruction in instructions: 
        for c in instruction:
            if c=='U':
                if position[0]-1>=0 \
                   and keypad[position[0]-1][position[1]]!='.': position[0]-=1
            if c=='D':
                if position[0]+1<=len(keypad)-1 \
                   and keypad[position[0]+1][position[1]]!='.': position[0]+=1
            if c=='L':
                if position[1]-1>=0 \
                   and keypad[position[0]][position[1]-1]!='.': position[1]-=1
            if c=='R':
                if position[1]+1<=len(keypad)-1 \
                   and keypad[position[0]][position[1]+1]!='.': position[1]+=1
        bathroom_code+=keypad[position[0]][position[1]]
    return bathroom_code


print("Part1 -- What is the bathroom code? "+str(get_bathroom_code(instructions,keypad1)))
print("Part2 -- What is the correct bathroom code? "+str(get_bathroom_code(instructions,keypad2)))
        
    
