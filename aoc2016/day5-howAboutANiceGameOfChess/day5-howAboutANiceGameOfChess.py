import hashlib

door_id="wtnhxymk"

password=""
password_part2=['','','','','','','','']

for x in range(0000000,90000000):
    if len(password)==8 and len(''.join(password_part2))==8: break
    s=door_id+'{0:05}'.format(x)
    hash_s= hashlib.md5(s.encode("utf-8")).hexdigest()
    hash_s_zeros=hash_s[:5]
    if hash_s_zeros == "00000":
        if len(password)!=8: password+=hash_s[5]
        index=hash_s[5]
        if index.isdigit():
            if int(index)<=7 and password_part2[int(index)]=='':
                password_part2[int(index)]=hash_s[6] 

print("Part1 -- Given the actual Door ID, what is the password? " +password)
print("Part2 -- Given the actual Door ID and this new method, what \
is the password? "+str(''.join(password_part2)))
