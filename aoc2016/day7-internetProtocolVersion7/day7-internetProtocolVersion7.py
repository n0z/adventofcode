import time

def get_hypernet_indexes(string:str)-> list:
    starts=[]; ends=[]
    for i in range(len(string)):
        if string[i]=='[': starts.append(i)
        if string[i]==']': ends.append(i)
    hypernets=[]
    for i in range(0,len(starts)):
        hypernets.append((starts[i],ends[i]))
    return hypernets

def get_hypernet_secuences(string:str) -> list:
    secuences=[]
    hypernets_index=get_hypernet_indexes(string)
    for i in range(len(hypernets_index)):
        start,end=hypernets_index[i]
        secuences.append(ip[start+1:end])
    return secuences

def get_normal_secuences(string: str) -> list:
    secuences=[]; s=""
    for i in range(len(string)):
        if string[i]=='[': secuences.append(s); s=""
        if string[i] ==']': s=""
        if string[i]!='[' and string[i]!=']': s+=string[i]
    secuences.append(s)
    return secuences

def detect_abba_pattern(string: str)-> bool:
    c=0
    while c<len(string):
        if c+1<len(string) and string[c]==string[c+1]:
            if c-1>=0 and string[c-1]!=string[c] and c+2<len(string) and string[c+2]==string[c-1]:
                return True
        c+=1
    return False

def get_aba_patterns(string: str) -> list:
    patterns=[]; c=0
    while c<len(string):
        if c+1<len(string) and string[c]!=string[c+1]:
            if c+2<len(string) and string[c]==string[c+2]:
                pattern=string[c]+string[c+1]+string[c+2]
                patterns.append(pattern)
        c+=1
    return patterns
        
with open("input","r") as f:
    ips=f.read().split("\n")

supports_TLS=0
supports_SSL=0
for ip in ips:
    hypernets=get_hypernet_secuences(ip)
    normals=get_normal_secuences(ip)
    hypernet_abba=False; normals_abba=False
    normals_aba=set(); hypernets_aba=set()
    for normal in normals:
        n_abas=get_aba_patterns(normal) 
        for aba in n_abas: normals_aba.add(aba)
        if detect_abba_pattern(normal): normals_abba=True
    for hypernet in hypernets:
        h_abas=get_aba_patterns(hypernet)
        for aba in h_abas: hypernets_aba.add(aba)
        if detect_abba_pattern(hypernet): hypernet_abba=True

    if not hypernet_abba and normals_abba: supports_TLS+=1 
    for normal_aba in normals_aba: 
        rev_aba=normal_aba[1]+normal_aba[0]+normal_aba[1]
        if  rev_aba in hypernets_aba: supports_SSL+=1; break

print("Part1 -- How many IPs in your puzzle input support TLS? "
      +str(supports_TLS))
print("Part2 -- How many IPs in your puzzle input support SSL? "
      +str(supports_SSL))
        
            
            
