
def create_empty_grid(wide:int,tall:int)->list:
    grid=[]
    for row in range(tall):
        l=[]
        for col in range(wide):
            l.append('.')
        grid.append(l)
    return grid

def rect(wide:int, tall:int, grid:list):
    for row in range(tall):
        for col in range(wide):
            grid[row][col]="#"

def rotate_column(column: int, size:int,grid:list):
    column_state=[grid[row][column] for row in range(len(grid))]
    column_state=column_state[-size:]+column_state[:-size]
    for row in range(len(grid)):
        grid[row][column]=column_state[row]
    
def rotate_row(row: int, size:int, grid:list):
    grid[row]=grid[row][-size:]+grid[row][:-size]

    
with open("input","r") as f:
    lines=f.read().split("\n") 

grid=create_empty_grid(50,6)

for line in lines:
    line=line.split()
    command=line[0]
    if command=="rect": 
        wide,tall=line[1].split('x'); wide=int(wide); tall=int(tall)
        rect(wide,tall,grid)
    else:
        rtype=line[1]
        size=int(line[4])
        if rtype=="row":
            row=int(line[2].split('=')[1]); rotate_row(row,size,grid)
        else:
            column=int(line[2].split('=')[1])
            rotate_column(column,size,grid)
lits=0
for r in range(len(grid)):
    for c in range(len(grid[0])):
        if grid[r][c]=='#': lits+=1
print("Part1 -- How many pixels should be lit? "+str(lits))
print("Part2 -- What code is the screen trying to display? ")
for r in range(len(grid)):
    s=""
    for c in range(len(grid[0])):
        if grid[r][c]=='.': grid[r][c]=' '
        s+=grid[r][c]
    print(s)
