with open("input","r") as f:
   polymer=f.read() 

polymer_size=len(polymer)
def reacting(polymer:str) -> list:
    stack=[]; stack.append(polymer[0])
    for unit in polymer[1:]:
        if len(stack)>0 and abs(ord(unit)-ord(stack[-1]))==32: stack.pop()
        else: stack.append(unit)
    return stack

print("Part1 -- How many units remain after fully reacting the polymer\
you scanned? "+str(len(reacting(polymer))))

min=99999
for i in range(97,123):
    new_polymer=polymer.replace(chr(i),'').replace(chr(i-32),'')
    stack=reacting(new_polymer)
    if len(stack)<min: min=len(stack) 
print("Part2 -- What is the length of the shortest polymer you can \
produce by removing all units of exactly one type and fully \
reacting the result? "+str(min))
    
