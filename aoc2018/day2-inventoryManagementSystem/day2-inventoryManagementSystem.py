with open("input","r") as f:
    boxes=f.read().split("\n")

two_letters=0
three_letters=0
for box in boxes:
    letters={}
    for letter in box:
       letters[letter]=box.count(letter) 
    two=0
    three=0
    for k,v in letters.items():
        if v==2: two=1 
        if v==3: three=1
    two_letters+=two; three_letters+=three
print("Part1 -- What is the checksum for your list of box IDs? "
      +str(two_letters*three_letters))

i=0; j=1
cbox1=""; cbox2=""
boxes_found=False
while i<len(boxes) and  not boxes_found:
    actual_box=boxes[i]
    while j<len(boxes):
        next_box=boxes[j]
        diff=0
        c=0
        while c<len(actual_box):
            if diff>1: break
            if actual_box[c]!=next_box[c]: diff+=1
            c+=1
        if diff==1:
            boxes_found=True
            cbox1=actual_box; cbox2=next_box
            break
        else:j+=1
    i+=1
    j=i+1
commons=""; c=0
while c<len(cbox1):
    if cbox1[c]==cbox2[c]: commons+=cbox1[c]
    c+=1
print("Part2 -- What letters are common between the two correct box IDs? "
      +commons)
