with open("input","r") as f:
    secuence=f.read().split("\n")

frecuency=sum([int(num) for num in secuence])
print("Part1 -- what is the resulting frequency after all of the \
changes in frequency have been applied? "+str(frecuency))
frecuency=0;frecuencies=set()
repeated=False
while not repeated: 
    for num in secuence:
        frecuency+=int(num) 
        if frecuency not in frecuencies: frecuencies.add(frecuency)
        else: repeated=True;  break
print("Part2 -- What is the first frequency your device reaches twice? "
      +str(frecuency))
