with open("input","r") as f:
    lines=f.read().split("\n")
lines.sort()

guards={}
actual_guard=""
falls=-1; wakes=-1
for line in lines:
    things=line.split()
    time=things[1]; minute=time.split(':')[1][0:2]
    if len(things)==6:
        guard=things[3]
        actual_guard=guard
        if guards.get(guard)==None:
            minutes={}
            for i in range(0,60): minutes[i]=0
            guards[guard]=[0,minutes]
            wakes=-1; falls=-1
    else:
        action=things[2]
        if action=="wakes": wakes=int(minute)
        elif action=="falls": falls=int(minute)
        if wakes!=-1 and falls!=-1:
            guards[actual_guard][0]+=abs(wakes-falls)
            for i in range(falls,wakes):
                guards[actual_guard][1][i]+=1
            wakes=-1;falls=-1

guard=""
sleep=0
for k,v in guards.items():
    if v[0]>sleep: guard=k; sleep=v[0]

minute=-1
times=0

for k,v in guards.get(guard)[1].items():
   if v>times: minute=k; times=v 
print("Part1 -- What is the ID of the guard you chose multiplied \
by the minute you chose? "+str(int(guard[1:])*minute))

asleep={}
for k,v in guards.items():
    guard=k
    max_min=-1
    value=0
    for k,v in guards.get(k)[1].items():
        if v>value: max_min=k; value=v
    asleep[guard]=(max_min,value)

maxv=0
guard=""
minutes=0
for k,v in asleep.items():
    if v[1]>maxv: maxv=v[1]; guard=k; minutes=v[0]
print("Part2 -- What is the ID of the guard you chose multiplied by \
the minute you chose? "+str(int(guard[1:])*minutes))


