
def create_empty_grid() -> list:
    grid=[]
    for row in range(0,1000):
        l=[]
        for col in range(0,1000):
            l.append('.') 
        grid.append(l)
    return grid

def parse(claim: str) -> list:
    other,size=claim.split(':')
    nid,separation=other.split('@')
    nid=nid.strip()
    le,te=separation.split(',')
    le=le.strip(); te=te.strip()
    wide,tall=size.split('x')
    wide=wide.strip(); tall=tall.strip()
    specs=[le,te,wide,tall,nid[1:]] 
    return specs

    
def draw_claim(claim: list,grid:list):
    le=int(claim[0]); te=int(claim[1]); wide=int(claim[2])
    tall=int(claim[3]); nid=claim[4]
    for row in range(te,te+tall):
        for col in range(le,le+wide):
            if grid[row][col]!='.': grid[row][col]='X'
            elif grid[row][col]=='.': grid[row][col]=nid

def count_multi_claims(grid: list) -> int:
    count=0
    for row in range(0,len(grid)):
        for col in range(0,len(grid[row])):
            if grid[row][col]=='X': count+=1
    return count

def search_not_overlap(claims: list, grid: list) -> str:
   for claim in claims:
      specs=parse(claim)
      le=int(specs[0]); te=int(specs[1]); wide=int(specs[2])
      tall=int(specs[3]); nid=specs[4]
      num_inches=wide*tall
      count=0
      for row in range(te,te+tall):
        for col in range(le,le+wide):
            if grid[row][col]==nid: count+=1
      if count==num_inches: return nid
    
with open("input","r") as f:
    claims=f.read().split("\n")

grid = create_empty_grid()    
for claim in claims:
    specs=parse(claim)
    draw_claim(specs,grid) 

    
print("Part1 -- How many square inches of fabric are within two \
or more claims? "+str(count_multi_claims(grid)))
print("Part2 -- What is the ID of the only claim that doesn't overlap? "
      +str(search_not_overlap(claims,grid)))
