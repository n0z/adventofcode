with open("input","r") as f:
   instructions = f.read().split("\n")
instructions=[[inst.split(),0] for inst in instructions]

def calculate_acc_part1(instructions: list) -> int:
   acc=0
   i=0
   while instructions[i][1] < 2:
      if instructions[i][0][0] == "acc":
         acc+=int(instructions[i][0][1])
         i+=1
      elif instructions[i][0][0] == "jmp":
         i+=int(instructions[i][0][1])
      elif instructions[i][0][0] == "nop":
         i+=1
      instructions[i][1]+=1
   return acc


print("Part1 -- What value is in the accumulator? "
      +str(calculate_acc_part1(instructions)))
