with open("input","r") as f:
    passports=f.read().split("\n\n")
passports=[[p for p in passport.split()] for passport in passports]

def validate_part1(passports: list) -> int:
    valids=0
    for passport in passports:
        if len(passport)==8: valids+=1
        elif len(passport)==7:
            cid=False
            for field in passport:
                if field.__contains__("cid"): cid=True; break
            if not cid: valids+=1

    return valids

def validate_part2(passports: list) -> int:
    valids=0
    eye_colors=["amb","blu","brn","gry","grn","hzl","oth"]
    for passport in passports: 
        if len(passport)>=7:
            fields=0
            for field in passport:
                name,value=field.split(":")
                if name=="byr":
                    if int(value)>=1920 and int(value)<=2002: fields+=1
                elif name=="iyr":
                    if int(value)>=2010 and int(value)<=2020: fields+=1
                elif name=="eyr":
                    if int(value)>=2020 and int(value)<=2030: fields+=1
                elif name=="hgt": 
                    typeh=value[-2:]
                    if typeh=="cm":
                        num=int(value[:-2])
                        if num>=150 and num<=193: fields+=1
                    elif typeh=="in":
                        num=int(value[:-2])
                        if num>=59 and num<=76: fields+=1
                elif name=="hcl":
                    if value[0]=="#" and len(value[1:])==6:
                        s=value[1:]
                        try: int(s,16); fields+=1
                        except ValueError: pass
                elif name=="ecl":
                    if value in eye_colors: fields+=1
                elif name=="pid":
                    if value.isdigit() and len(value)==9: fields+=1
            if fields>=7: valids+=1
    return valids

print("Part1 -- In your batch file, how many passports are valid? "
      +str(validate_part1(passports)))
print("Part2 -- In your batch file, how many passports are valid? "
      +str(validate_part2(passports)))
