with open("input","r") as f:
    numbersO = f.read().split()
numbers={}
for number in numbersO:
    numbers[number]=int(number)

n1,n2=0,0
for number in numbersO:
    s=2020-int(number)
    if numbers.get(str(s))!=None: n1=int(number); n2=numbers.get(str(s)); break

print("Part1 -- Find the two entries that sum to 2020 and multiply together: ")
print("\tnumbers are: "+str(n1)+", "+str(n2))
print("\tThe multiplication is: "+str(n1*n2))
print("\n")
nu1,nu2,nu3=0,0,0
for n1 in numbersO:
    for n2 in numbersO:
        for n3 in numbersO:
            if int(n1)+int(n2)+int(n3)==2020: nu1=n1;nu2=n2;nu3=n3
print("Part2 -- Find the two entries that sum to 2020; what do you get \
if multiply them together?")
print("\tnumbers are: "+str(nu1)+", "+str(nu2)+", "+str(nu3))
print("\tThe multiplication is: "+str(int(nu1)*int(nu2)*int(nu3)))

