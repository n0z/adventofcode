with open("input","r") as f:
    database=f.read().split("\n")


def parse(line: str) -> int and int and str and str:
    policy,password=line.split(":")
    password=password.strip()
    letter=policy.split()[1]
    minl,maxl=policy.split()[0].split("-"); minl=int(minl);maxl=int(maxl)
    return minl,maxl,letter,password 

valids_part1=0
valids_part2=0
for line in database:
    minl,maxl,letter,password=parse(line)
    if minl<=password.count(letter)<=maxl: valids_part1+=1
    if (
        (password[minl-1]==letter and password[maxl-1]!=letter) or
        (password[minl-1]!= letter and password[maxl-1]==letter)
    ): valids_part2+=1
            

print("Part1 -- How many passwords are valid according to their policies? "
      +str(valids_part1))

print("Part2 -- How many passwords are valid according to the new interpretation of the policies? "+str(valids_part2))
