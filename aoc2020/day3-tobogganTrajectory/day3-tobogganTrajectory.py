with open("input","r") as f:
    lines=f.read().split()

def find_trees(lines: list, slopes: list) -> int:
    trees_list=[]
    for slope in slopes:
        col_inc,row_inc=slope
        row=0; col=0
        trees=0
        while row<len(lines):
            col+=col_inc
            if col >= len(lines[row]): col=col%len(lines[row])
            row+=row_inc
            
            if row<len(lines) and lines[row][col] == "#":
                trees+=1
        trees_list.append(trees)

    if len(trees_list)==1: return trees
    else:
        res=1
        for tree in trees_list: res*=tree
        return res

print("Part1 -- How mnay trees would you encounter? "
      +str(find_trees(lines,[(3,1)])))

#Part 2
print("Part2 -- What do you get if you multiply together the number\
of trees encountered on each of the listed slopes? "
+str(find_trees(lines,[(1,1),(3,1),(5,1),(7,1),(1,2)])))
