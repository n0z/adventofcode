with open("input","r") as f:
   seats = f.read().split()

def get_seats_id(seats: list) -> list:
   code={"F":0,"B":1,"L":0,"R":1}; seats_id=[]
   for seat in seats:
      binary=""
      for c in seat: binary+=str(code.get(c))
      row=binary[:7]; row=int(row,2)
      col=binary[7:]; col=int(col,2)
      seats_id.append(row*8+col)
   return seats_id 

seats_full=get_seats_id(seats); seats_full.sort()
print("Part1 -- What is the highest seat ID on a boarding pass? "
      +str(seats_full[-1]))
i=0
while seats_full[i]+1!=seats_full[i+1]-1: i+=1
print("Part2 -- What is the ID of your seat? "+str(seats_full[i]+1))
