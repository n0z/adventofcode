with open("input","r") as f:
    groups=f.read().split("\n\n")

def count_anyone_awnser_yes(groups:list)->int:
    num_yes=0
    for group in groups:
        yes=set();
        for answer in group:
            if answer!="\n":yes.add(answer)
        num_yes+=len(yes)
    return num_yes

def count_everyone_awnser_yes(groups:list) -> int:
    count=0
    for group in groups:
        size=len(group.split())
        d={}
        if size==1: count+=len(group)
        else:
            for row in group.split():
                for c in row:
                    if d.get(c)==None: d[c]=1
                    else: d[c]+=1

        for c,v in d.items():
            if v==size: count+=1
    return count
            
print("Part1 -- What is the sum of those counts? "
      +str(count_anyone_awnser_yes(groups)))
print("Part2 -- What is the sum of those counts? "
      +str(count_everyone_awnser_yes(groups)))
    
